# Development

Want to contribute? Great!

We uses Gulp for fast developing.
Make a change in your file and instantanously see your updates!

Open your favorite Terminal and run these commands.

Go to folder smart-study located in wp-contant > themes.
```sh
$ cd wp-content
$ cd themes
$ cd smart-study
```

Then install all packages by running 1 command:
```sh
$ npm install
```

Congrats! Now you can start work with running this command:
```sh
$ gulp start
```
Great! Live reload and SASS converting is configured.

## Building for source
How to prepare CSS for production release?

Minify you css by running:
```sh
$ gulp minify-css
```


(optional) For clean up main.css use:
```sh
$ gulp build
```
*It can be used as alternative for minifing, because it have built-in minify.

(optional) To add all CSS files to main.css:
```sh
$ gulp concat
```


Also we need to prepare JavaScript.

Similar to CSS step of uniting separated .js files and minifing final file in 1 command:
```sh
$ gulp minify-js
```

(optional) We can run .js concat without minifing:
```sh
$ gulp build-scripts
```

Heroku upload:

```sh
$ git add .
$ git commit -m "Start with create-react-app"
$ git push heroku master
```

Full Heroku guide:

```sh
$ npm install heroku
$ create-react-app $APP_NAME
$ cd $APP_NAME
$ git init
$ heroku create $APP_NAME --buildpack https://github.com/mars/create-react-app-buildpack.git
$ git add .
$ git commit -m "Start with create-react-app"
$ git push heroku master
$ heroku open
```