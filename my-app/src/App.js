import React, { Component } from 'react';
import Projects from './components/Projects';
import AddProject from './components/addProject';
import PostList from './components/PostList'
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

class App extends Component {

  render() {
    return (
      <div className="App">
          <AddProject />
          <Projects>Loading...</Projects>
      </div>
    );
  }
}
class Posts extends Component {

    render() {
        return (
            <div className="Posts">
                Post list:
                <PostList />
            </div>
        );
    }
}

const BasicExample = () => (
    <Router>
        <div className="menu">
            <ul>
                <li>
                    <Link to="/">Home</Link>
                </li>
                <li>
                    <Link to="/about">Userlist</Link>
                </li>
                <li>
                    <Link to="/topics">Posts</Link>
                </li>
            </ul>

            <hr />

            <Route exact path="/" component={Home} />
            <Route path="/about" component={App} />
            <Route path="/topics" component={Posts} />
        </div>
    </Router>
);

const Home = () => (
    <div>
        <h2>Home</h2>
    </div>
);

export default BasicExample;