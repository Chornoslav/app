import React, { Component } from 'react';

class ProjectItem extends Component {

    render() {
        return (
            <li className="Project">
                <div className={this.props.gender}><img src={this.props.thumbnail} alt={this.props.name}/></div>
                <div className="user-info">
                    <p>Name:{this.props.name};</p>
                    <p>Location:{this.props.location}</p>
                    <p>Email:{this.props.email}</p>
                </div>
            </li>
        );
    }
}

export default ProjectItem;