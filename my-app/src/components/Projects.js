import React, { Component } from 'react';
import ProjectItem from "./project-item";


class Projects extends Component {
    constructor(props){
        super(props);
        this.state = {
            contacts: []
        }
    }
    componentDidMount() {
        this.fetchData();
    }
    fetchData(){
        this.setState({
            contacts: []
        })

        fetch('https://randomuser.me/api/?results=50&nat=us,dk,fr,gb')
            .then(response => response.json())
            .then(parsedJSON => parsedJSON.results.map(user => (
                {
                    name: `${user.name.first} ${user.name.last}`,
                    username: `${user.login.username}`,
                    email: `${user.email}`,
                    thumbnail: `${user.picture.medium}`,
                    gender: `${user.gender}`,
                    location: `${user.location.street}, ${user.location.city}`
                }
            )))
            .then(contacts => this.setState({
                contacts
            }))
            .catch(error => console.log('parsing failed', error))
    }
    render() {

        const {contacts} = this.state;
        function _gender() {
            console.log('Click');
            contacts.sort(function (a, b) {
                let genderA = a.gender.toUpperCase();
                let genderB = b.gender.toUpperCase();
                if (genderA < genderB) {
                    return -1;
                }
                if (genderA > genderB) {
                    return 1;
                }
                return 0;
            });
        }
        return (
            <div className="Projects">
                <button onClick = {() => this.setState.sort(function (a, b) {
                    let genderA = a.gender.toUpperCase();
                    let genderB = b.gender.toUpperCase();
                    if (genderA < genderB) {
                        return -1;
                    }
                    if (genderA > genderB) {
                        return 1;
                    }
                    return 0;
                })} className="btn3">Sort by gender</button>
                <button onClick={_gender} className="btn3">Sort by name</button>
                Userlist:
                <ol>
                    {
                        contacts.map(contact => {

                            const {username, name, email, location, thumbnail, gender} = contact;

                            return <ProjectItem key={username} name={name} email={email}
                                                location={location} thumbnail={thumbnail} gender={gender}/>
                        })
                    }
                </ol>
            </div>
        );
    }
}

export default Projects;
