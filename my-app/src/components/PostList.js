import React, { Component } from 'react';
import Post from './Post';



class PostList extends Component {
    constructor(props){
        super(props);
        this.state = {
            posts: []
        }
    }
    componentDidMount() {
        this.fetchData();
    }
    fetchData(){
        this.setState({
            posts: []
        })

        fetch('http://www.json-generator.com/api/json/get/bUiIKTPZfm')
            .then(response => response.json())
            .then(parsedJSON => parsedJSON.map(user => (
                {
                    name: `${user.name}`,
                    fruit: `${user.favoriteFruit}`,
                    content: `${user.about}`,
                    username: `${user._id}`
                }
            )))
            .then(posts => this.setState({
                posts
            }))
            .catch(error => console.log('parsing failed', error))
    }
    render() {

        const {posts} = this.state;

        return (
            <div className="Projects">
                <ol>
                    {
                        posts.map(posts => {

                            const {fruit, name, content, username} = posts;

                            return <Post key={username} name={name} fruit={fruit} content={content}/>
                        })
                    }
                </ol>
            </div>
        );
    }
}

export default PostList;
