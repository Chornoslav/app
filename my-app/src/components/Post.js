import React, { Component } from 'react';

class Post extends Component {

    render() {
        return (
            <li className="Post">
                <div className="post-info">
                    <p>{this.props.name};</p>
                    <p>{this.props.fruit}</p>
                </div>
                <div className="post-content">
                    <p>{this.props.content}</p>
                </div>
            </li>
        );
    }
}

export default Post;